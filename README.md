# DataAnalytics_1

# WINDOWS

# Установить Python
python -m pip install --upgrade pip
# Установить Jupyter-notebook
pip install notebook
# Установка библиотеки PANDAS для работы с табличными данными (можно не писать "python -m")
python -m pip install pandas
python -m pip show pandas
# Установка библиотеки SEABORN для работы с графиками (можно не писать "python -m")
python -m pip install seaborn
python -m pip show pandas


# LINUX

# Создаем алиас на 3 версию python (вместо второй)
echo "alias python=/usr/bin/python3" >> ~/.bashrc
# Применение настроек
source ~/.bashrc
# Установить Jupyter-notebook
pip3 install notebook
# Установка LINUX библиотеки PANDAS для работы с табличными данными
sudo pip3 install pandas
sudo pip3 show pandas
# Устранение ошибки(Original error was: libf77blas.so.3: cannot open shared object file: No such file or directory) 
sudo apt-get install libatlas-base-dev
# Установка библиотеки SEABORN для работы с графиками
sudo pip3 install seaborn
sudo pip3 show pandas
